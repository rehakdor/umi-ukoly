#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>

#define CONTAINER_OF(structure, member, pointer) \
    ((structure *)(((char *)(pointer)) - offsetof(structure, member)))
void *xmalloc(size_t size){
    void *result = malloc(size);
    if(!result)
        abort();
    return result;
}
struct dlist{
    struct dlist *next;
    struct dlist *prev;
};
#define DLIST_INIT(name) \
    { &(name), &(name) }
#define DLIST_CREATE(name) \
    struct dlist name = DLIST_INIT(name)
#define DLIST_FOREACH(pos, head) \
    for((pos) = (head)->next; (pos) != (head); (pos) = (pos)->next)
#define DLIST_FOREACH_SAFE(pos, temp, head) \
    for((pos) = (head)->next, (temp) = (pos)->next; (pos) != (head); (pos) = (temp), (temp) = (pos)->next)

void dlist_init(struct dlist *list){
    list->prev = list;
    list->next = list;
}
void dlist_addtail(struct dlist *next, struct dlist *new_){
    new_->prev = next->prev;
    new_->next = next;
    next->prev->next = new_;
    next->prev = new_;
}

struct uf_node{
    struct uf_node *parent;
    unsigned int rank;
};
void uf_node_init(struct uf_node *node){
    node->parent = NULL;
    node->rank = 1;
}
struct uf_node *uf_node_find(struct uf_node *node){
    while(node->parent)
        node = node->parent;
    return node;
}
void uf_node_union(struct uf_node *a, struct uf_node *b){
    struct uf_node *ap = uf_node_find(a);
    struct uf_node *bp = uf_node_find(b);
    if(ap == bp)
        return;
    if(ap->rank < bp->rank){
        struct uf_node *tmp = ap;
        ap = bp;
        bp = tmp;
    }
    bp->parent = ap;
    if(ap->rank == bp->rank)
        ap->rank++;
}

struct pair{
    struct dlist list_node;
    unsigned int left_var;
    unsigned int right_var;
};
struct pair *pair_new(unsigned int left_var, unsigned int right_var){
    struct pair *result = xmalloc(sizeof(struct pair));
    result->left_var = left_var;
    result->right_var = right_var;
    dlist_init(&result->list_node);
    return result;
}

DLIST_CREATE(equals);
DLIST_CREATE(unequals);
struct uf_node *uf_nodes;

bool read_pair(unsigned int *highest_index, FILE *file){
    unsigned int left_var;
    unsigned int right_var;
    char op;
    if(fscanf(file, " %u %c %u", &left_var, &op, &right_var) != 3)
        return false;

    struct pair *result = pair_new(left_var, right_var);
    if(left_var > *highest_index)
        *highest_index = left_var;
    if(right_var > *highest_index)
        *highest_index = right_var;

    if(op == '=')
        dlist_addtail(&equals, &result->list_node);
    else if(op == '!')
        dlist_addtail(&unequals, &result->list_node);
    else{
        fprintf(stderr, "Invalid operand\n");
        abort();
    }
    return true;
}
int main(int argc, char *argv[]){
    if(argc < 2){
        fprintf(stderr, "Chybi argument s cestou vstupniho souboru\n");
        return 1;
    }

    unsigned int highest_index = 0;
    FILE *file = fopen(argv[1], "rt");
    while(read_pair(&highest_index, file));
    fclose(file);
    if(highest_index == 0){
        fprintf(stderr, "Nepodarilo se precist zadnou (ne)rovnici\n");
        return 1;
    }

    uf_nodes = xmalloc(sizeof(struct uf_node) * (highest_index + 1));
    for(size_t i = 0; i < highest_index + 1; i++)
        uf_node_init(&uf_nodes[i]);

    struct dlist *curr;
    DLIST_FOREACH(curr, &equals){
        struct pair *pair = CONTAINER_OF(struct pair, list_node, curr);
        printf("Zpracovavani rovnice: %u = %u\n", pair->left_var, pair->right_var);
        uf_node_union(&uf_nodes[pair->left_var], &uf_nodes[pair->right_var]);
    }
    struct pair *failed_pair = NULL;
    DLIST_FOREACH(curr, &unequals){
        struct pair *pair = CONTAINER_OF(struct pair, list_node, curr);
        printf("Zpracovavani nerovnice: %u ! %u\n", pair->left_var, pair->right_var);
        struct uf_node *l = uf_node_find(&uf_nodes[pair->left_var]);
        struct uf_node *r = uf_node_find(&uf_nodes[pair->right_var]);
        if(l == r){
            failed_pair = pair;
            break;
        }
    }

    printf("\n");
    if(failed_pair)
        printf("Vyraz neni splnitelny\nPrvni selhana nerovnice: %u ! %u\n", failed_pair->left_var, failed_pair->right_var);
    else
        printf("Vyraz je splnitelny\n");

    struct dlist *tmp;
    DLIST_FOREACH_SAFE(curr, tmp, &equals)
        free(curr);
    DLIST_FOREACH_SAFE(curr, tmp, &unequals)
        free(curr);
    free(uf_nodes);

    return 0;
}
