#include <iostream>
#include <map>
#include <vector>

#include "solver.cpp"

#define DIM 3

class CoordSudoku : public Coord{
public:
    unsigned int x, y;
    CoordSudoku(unsigned int p_x, unsigned int p_y)
        : x(p_x), y(p_y){
    }
    string print(void) const override{
        return "CoordSudoku(" + std::to_string(x) + "," + std::to_string(y) + ")";
    }
};

class ValueSudoku : public Value{
public:
    unsigned int v;
    ValueSudoku(unsigned int p_v)
        : v(p_v){
    }
    string print(void) const override{
        return "ValueSudoku(" + std::to_string(v) + ")";
    }
};

class ConstraintSudoku : public Constraint{
public:
    using Constraint::Constraint;
    bool judge(Value *left, Value *right) override{
        ValueSudoku *rleft = static_cast<ValueSudoku *>(left);
        ValueSudoku *rright = static_cast<ValueSudoku *>(right);
        return rleft->v != rright->v;
    }
    Constraint *clone(void) const override{
        return new ConstraintSudoku(left, right);
    }
};

class PresetField{
public:
    unsigned int x;
    unsigned int y;
    unsigned int val;

    PresetField(unsigned int p_x, unsigned int p_y, unsigned int p_val)
        : x(p_x), y(p_y), val(p_val){
    }
};

int main(int argc, char *argv[]){
    World world;
    // All the possible coordinates
    for(unsigned int y = 1; y <= DIM*DIM; y++){
        for(unsigned int x = 1; x <= DIM*DIM; x++){
            world.coords.push_back(new CoordSudoku(x, y));
        }
    }
    // All the possible values
    for(unsigned int v = 1; v <= DIM*DIM; v++)
        world.values.push_back(new ValueSudoku(v));
    // All the constraints... there is so many
    for(unsigned int y = 0; y < DIM*DIM; y++){
        for(unsigned int x = 0; x < DIM*DIM; x++){
            unsigned int curr_index = y*DIM*DIM+x;
            for(unsigned int sx = 0; sx < DIM*DIM; sx++){
                unsigned int tgt_index = y*DIM*DIM+sx;
                if(sx != x && curr_index < tgt_index)
                    world.constraints.push_back(new ConstraintSudoku(world.coords[curr_index], world.coords[tgt_index]));
            }
            for(unsigned int sy = 0; sy < DIM*DIM; sy++){
                unsigned int tgt_index = sy*DIM*DIM+x;
                if(sy != y && curr_index < tgt_index)
                    world.constraints.push_back(new ConstraintSudoku(world.coords[curr_index], world.coords[tgt_index]));
            }
            for(unsigned int sy = y / DIM * DIM; sy < y / DIM * DIM + DIM; sy++){
                for(unsigned int sx = x / DIM * DIM; sx < x / DIM * DIM + DIM; sx++){
                    unsigned int tgt_index = sy*DIM*DIM+sx;
                    if((sx != x || sy != y) && curr_index < tgt_index){
                        world.constraints.push_back(new ConstraintSudoku(world.coords[curr_index], world.coords[tgt_index]));
                    }
                }
            }
        }
    }

    // Preset fields, cannot be changed during resolution
    vector<PresetField> preset_fields = {
        { 1, 1, 5 },
        { 2, 1, 3 },
        { 5, 1, 7 },
        { 1, 2, 6 },
        { 4, 2, 1 },
        { 5, 2, 9 },
        { 6, 2, 5 },
        { 2, 3, 9 },
        { 3, 3, 8 },
        { 8, 3, 6 },
        { 1, 4, 8 },
        { 5, 4, 6 },
        { 9, 4, 3 },
        { 1, 5, 4 },
        { 4, 5, 8 },
        { 6, 5, 3 },
        { 9, 5, 1 },
        { 1, 6, 7 },
        { 5, 6, 2 },
        { 9, 6, 6 },
        { 2, 7, 6 },
        { 7, 7, 2 },
        { 8, 7, 8 },
        { 4, 8, 4 },
        { 5, 8, 1 },
        { 6, 8, 9 },
        { 9, 8, 5 },
        { 5, 9, 8 },
        { 8, 9, 7 },
        { 9, 9, 9 },
    };

    // Now put it all together
    Problem problem;
    // Preset fields can only have the preset value; we set them first so they are filled first
    for(auto pf : preset_fields){
        Coord *coord = world.coords[(pf.y-1)*DIM*DIM+(pf.x-1)];
        problem.variables.push_back(coord);
        problem.domains[coord] = vector<Value *>{ world.values[pf.val - 1] };
    }
    // Variables can have all the values
    vector<Value *> all_values;
    for(auto v : world.values)
        all_values.push_back(v);
    for(auto c : world.coords){
        auto it = problem.domains.find(c);
        if(it == problem.domains.end()){ // hasnt been filled by presets yet
            problem.variables.push_back(c);
            problem.domains[c] = all_values;
        }
    }
    // Constraints are copied
    for(auto c : world.constraints)
        problem.constraints.push_back(c);

    Solver solver(world, problem);
    if(!solver.solve_ui())
        cout << "FAIL" << endl;
    else{
        cout << "SUCCESS" << endl << endl;
        solver.print();
    }
    cout << "Total assignments: " << solver.total_assignments << endl;

    return 0;
}
