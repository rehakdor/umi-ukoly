#include <iostream>
#include <map>
#include <vector>
#include <set>
#include <deque>
#include <utility>

using namespace std;

// Idenfifies a node (variable, preset value, ...)
class Coord{
public:
    // content doesnt matter, isnt used by solver
    virtual ~Coord(void){
    }
    virtual string print(void) const = 0;
};
// Individual value of a node
class Value{
public:
    virtual ~Value(void){
    }
    virtual string print(void) const = 0;
};
// Binary constraint
class Constraint{
public:
    Coord *left;
    Coord *right;
    Constraint(Coord *p_left, Coord *p_right)
        : left(p_left), right(p_right){
    }
    virtual ~Constraint(void){
    }
    virtual bool judge(Value *left, Value *right) = 0;
    virtual Constraint *clone(void) const = 0;
    string print(void) const{
        return "Constraint(" + left->print() + "," + right->print() + ")";
    }
};

// Contains all objects
class World{
public:
    vector<Coord *> coords;
    vector<Value *> values;
    vector<Constraint *> constraints;

    ~World(void){
        for(auto c : coords)
            delete c;
        for(auto v : values)
            delete v;
        for(auto c : constraints)
            delete c;
    }
};

// Specification of the problem to be solved
class Problem{
public:
    vector<Coord *> variables;
    map<Coord *, vector<Value *>> domains;
    vector<Constraint *> constraints;
};

// Solves things
class Solver{
public:
    World& world;
    Problem& problem;

    map<Coord *, Value *> mapping;
    unsigned int total_assignments;

    Solver(World& p_world, Problem& p_problem)
        : world(p_world), problem(p_problem), total_assignments(0){
    }
    bool passes_constraints(Coord *variable, Value *value, set<Coord *>& conflicts){
        conflicts.clear();
        conflicts.insert(variable);
        bool result = true;
        for(auto c : problem.constraints){
            if(c->left == variable){
                auto it = mapping.find(c->right);
                if(it != mapping.end()){
                    cout << "Checking: " << c->print() << endl;
                    if(!c->judge(value, it->second)){
                        result = false;
                        conflicts.insert(c->right);
                    }
                }
            }else if(c->right == variable){
                auto it = mapping.find(c->left);
                if(it != mapping.end()){
                    cout << "Checking: " << c->print() << endl;
                    if(!c->judge(it->second, value)){
                        result = false;
                        conflicts.insert(c->left);
                    }
                }
            }
        }
        return result;
    }
    bool revise(Constraint *c){
        bool deleted = false;
        auto& left_domain = problem.domains[c->left];
        for(auto it = left_domain.begin(); it != left_domain.end(); ){
            Value *vl = *it;
            bool found = false;
            for(Value *vr : problem.domains[c->right]){
                if(c->judge(vl, vr)){
                    found = true;
                    break;
                }
            }
            if(!found){
                deleted = true;
                it = left_domain.erase(it);
            }else
                ++it;
        }
        return deleted;
    }
    void ac3(void){
        deque<Constraint *> queue;
        for(Constraint *c : problem.constraints){
            Constraint *nc1 = c->clone();
            Constraint *nc2 = c->clone();
            nc2->left = nc1->right;
            nc2->right = nc1->left;
            queue.push_back(nc1);
            queue.push_back(nc2);
        }
        while(!queue.empty()){
            Constraint *c = queue.front();
            queue.pop_front();
            if(revise(c)){
                for(Constraint *d : problem.constraints){
                    if(c->left == d->right)
                        queue.push_back(d->clone());
                }
            }
            delete c;
        }
    }

    // Solve with backtracking
    bool solve_bt(void){
        if(mapping.size() == problem.variables.size())
            return true;
        Coord *current = problem.variables[mapping.size()];

        for(auto v : problem.domains[current]){
            cout << "Trying " << current->print() << " = " << v->print() << endl;
            set<Coord *> conflicts;
            if(passes_constraints(current, v, conflicts)){
                cout << "Passed" << endl;
                mapping[current] = v;
                total_assignments++;
                if(solve_bt())
                    return true;
                cout << "    BACK TRACK" << endl;
                mapping.erase(problem.variables[mapping.size() - 1]);
            }else
                cout << "Didnt pass" << endl;
        }
        cout << "    Tried all values, returning" << endl;
        return false;
    }

    // Solve with backjumping
    bool solve_bj_internal(set<Coord *>& out_conflicts){
        out_conflicts.clear();
        if(mapping.size() == problem.variables.size()){
            out_conflicts.clear();
            return true;
        }
        Coord *current = problem.variables[mapping.size()];

        bool success = false;
        set<Coord *> conflicts;
        for(auto v : problem.domains[current]){
            cout << "Trying " << current->print() << " = " << v->print() << endl;
            set<Coord *> new_conflicts;
            if(passes_constraints(current, v, new_conflicts)){
                cout << "Passes" << endl;
                mapping[current] = v;
                total_assignments++;
                success = solve_bj_internal(new_conflicts);
            }else{
                cout << "Didnt pass" << endl;
            }
            if(success){
                out_conflicts.clear();
                return true;
            }else if(new_conflicts.find(current) == new_conflicts.end()){
                cout << "    JUMPING" << endl;
                out_conflicts = new_conflicts;
                mapping.erase(problem.variables[mapping.size() - 1]);
                return false;
            }else{
                conflicts.merge(new_conflicts);
                conflicts.erase(current);
            }
        }
        cout << "    Tried all values, returning" << endl;
        out_conflicts = conflicts;
        mapping.erase(problem.variables[mapping.size() - 1]);
        return false;
    }
    bool solve_bj(void){
        set<Coord *> garbage;
        return solve_bj_internal(garbage);
    }
    // dispatcher
    bool solve_ui(void){
        char type;
        char ac3e;
        cout << "Select algorithm [t=backtrack, j=backjump]:" << endl;
        cin >> type;
        cout << "Use AC-3? [y=yes]" << endl;
        cin >> ac3e;
        if(ac3e == 'y')
            ac3();
        if(type == 't')
            return solve_bt();
        else if(type == 'j')
            return solve_bj();
        else{
            cerr << "Unknown algorithm requested" << endl;
            return false;
        }
    }

    void print(void){
        for(auto v : problem.variables){
            cout << v->print() << " = " << mapping[v]->print() << endl;
        }
    }
};
