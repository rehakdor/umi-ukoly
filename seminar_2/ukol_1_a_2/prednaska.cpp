#include <iostream>
#include <map>
#include <vector>

#include "solver.cpp"

class CoordPrednaska : public Coord{
public:
    string name;
    CoordPrednaska(const string& p_name)
        : name(p_name){
    }
    string print(void) const override{
        return "CoordPrednaska(" + name + ")";
    }
};

enum class Color{
    RED,
    GREEN,
    BLUE
};
class ValuePrednaska : public Value{
public:
    Color color;
    ValuePrednaska(Color p_color)
        : color(p_color){
    }
    string color_name(Color c) const{
        switch(c){
            case Color::RED:
            return "red";
            case Color::GREEN:
            return "green";
            case Color::BLUE:
            return "blue";
        }
        return ""; // for warnings
    }
    string print(void) const override{
        return "ValuePrednaska(" + color_name(color) + ")";
    }
};

class ConstraintPrednaska : public Constraint{
public:
    using Constraint::Constraint;
    bool judge(Value *left, Value *right) override{
        ValuePrednaska *rleft = static_cast<ValuePrednaska *>(left);
        ValuePrednaska *rright = static_cast<ValuePrednaska *>(right);
        return rleft->color != rright->color;
    }
    Constraint *clone(void) const override{
        return new ConstraintPrednaska(left, right);
    }
};

int main(int argc, char *argv[]){
    World world;
    auto c1 = new CoordPrednaska("c1");
    auto c2 = new CoordPrednaska("c2");
    auto c3 = new CoordPrednaska("c3");
    auto c4 = new CoordPrednaska("c4");
    auto c5 = new CoordPrednaska("c5");
    world.coords.push_back(c1);
    world.coords.push_back(c2);
    world.coords.push_back(c3);
    world.coords.push_back(c4);
    world.coords.push_back(c5);

    auto red = new ValuePrednaska(Color::RED);
    auto green = new ValuePrednaska(Color::GREEN);
    auto blue = new ValuePrednaska(Color::BLUE);
    world.values.push_back(red);
    world.values.push_back(green);
    world.values.push_back(blue);

    world.constraints.push_back(new ConstraintPrednaska(c1, c2));
    world.constraints.push_back(new ConstraintPrednaska(c1, c3));
    world.constraints.push_back(new ConstraintPrednaska(c1, c4));
    world.constraints.push_back(new ConstraintPrednaska(c1, c5));
    world.constraints.push_back(new ConstraintPrednaska(c2, c3));
    world.constraints.push_back(new ConstraintPrednaska(c3, c4));
    world.constraints.push_back(new ConstraintPrednaska(c3, c5));

    Problem problem;
    for(auto c : world.coords)
        problem.variables.push_back(c);
    problem.domains[c1] = { red, green, blue };
    problem.domains[c2] = { green, blue };
    problem.domains[c3] = { red, blue };
    problem.domains[c4] = { red, green, blue };
    problem.domains[c5] = { red, blue };
    for(auto c : world.constraints)
        problem.constraints.push_back(c);

    Solver solver(world, problem);
    if(!solver.solve_ui())
        cout << "FAIL" << endl;
    else{
        cout << "SUCCESS" << endl << endl;
        solver.print();
    }
    cout << "Total assignments: " << solver.total_assignments << endl;

    return 0;
}
