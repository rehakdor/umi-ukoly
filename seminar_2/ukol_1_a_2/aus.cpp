#include <iostream>
#include <map>
#include <vector>
#include <set>

#include "solver.cpp"

class CoordAus : public Coord{
public:
    string name;
    CoordAus(const string& p_name)
        : name(p_name){
    }
    string print(void) const override{
        return "CoordAus(" + name + ")";
    }
};

enum class Color{
    RED,
    GREEN,
    BLUE
};
class ValueAus : public Value{
public:
    Color color;
    ValueAus(Color p_color)
        : color(p_color){
    }
    string color_name(Color c) const{
        switch(c){
            case Color::RED:
            return "red";
            case Color::GREEN:
            return "green";
            case Color::BLUE:
            return "blue";
        }
        return ""; // for warnings
    }
    string print(void) const override{
        return "ValueAus(" + color_name(color) + ")";
    }
};

class ConstraintAus : public Constraint{
public:
    using Constraint::Constraint;
    bool judge(Value *left, Value *right) override{
        ValueAus *rleft = static_cast<ValueAus *>(left);
        ValueAus *rright = static_cast<ValueAus *>(right);
        return rleft->color != rright->color;
    }
    Constraint *clone(void) const override{
        return new ConstraintAus(left, right);
    }
};

int main(int argc, char *argv[]){
    World world;
    auto wa = new CoordAus("WA");
    auto nt = new CoordAus("NT");
    auto sa = new CoordAus("SA");
    auto q = new CoordAus("Q");
    auto nsw = new CoordAus("NSW");
    auto v = new CoordAus("V");
    auto t = new CoordAus("T");
    world.coords.push_back(wa);
    world.coords.push_back(nt);
    world.coords.push_back(sa);
    world.coords.push_back(q);
    world.coords.push_back(nsw);
    world.coords.push_back(v);
    world.coords.push_back(t);

    auto red = new ValueAus(Color::RED);
    auto green = new ValueAus(Color::GREEN);
    auto blue = new ValueAus(Color::BLUE);
    world.values.push_back(red);
    world.values.push_back(green);
    world.values.push_back(blue);

    world.constraints.push_back(new ConstraintAus(wa, nt));
    world.constraints.push_back(new ConstraintAus(wa, sa));
    world.constraints.push_back(new ConstraintAus(nt, sa));
    world.constraints.push_back(new ConstraintAus(nt, q));
    world.constraints.push_back(new ConstraintAus(sa, q));
    world.constraints.push_back(new ConstraintAus(sa, nsw));
    world.constraints.push_back(new ConstraintAus(sa, v));
    world.constraints.push_back(new ConstraintAus(q, nsw));
    world.constraints.push_back(new ConstraintAus(nsw, v));

    Problem problem;
    for(auto c : world.coords)
        problem.variables.push_back(c);
    vector<Value *> all_values;
    for(auto v : world.values)
        all_values.push_back(v);
    for(auto v : problem.variables){
        problem.domains[v] = all_values;
    }
    for(auto c : world.constraints)
        problem.constraints.push_back(c);

    Solver solver(world, problem);
    if(!solver.solve_ui())
        cout << "FAIL" << endl;
    else{
        cout << "SUCCESS" << endl << endl;
        solver.print();
    }
    cout << "Total assignments: " << solver.total_assignments << endl;

    return 0;
}
